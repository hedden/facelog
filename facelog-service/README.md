# facelog-service


定义并实现facelog 服务接口(thrift service名`IFaceLog`)

[src/sql2java](src/sql2java)下的代码由sql2java自动生成

服务接口定义：[net.gdface.facelog.service.BaseFaceLog](src/main/java/net/gdface/facelog/service/BaseFaceLog.java "BaseFaceLog")

服务接口实现：[net.gdface.facelog.service.FaceLogImpl](src/main/java/net/gdface/facelog/service/FaceLogImpl.java "FaceLogImpl")
